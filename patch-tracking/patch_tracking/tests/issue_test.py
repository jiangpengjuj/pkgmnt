import unittest
import json
from patch_tracking.app import app
from patch_tracking.api.business import create_issue
from patch_tracking.database import reset_db


class TestIssue(unittest.TestCase):
    '''
        issue测试
    '''
    def setUp(self) -> None:
        self.client = app.test_client()
        reset_db.reset()

    def test_none_data(self):
        with app.app_context():
            data = {
                "repo": "D",
                "branch": "D",
            }

            resp = self.client.get("/issue", data=data, content_type="application/json")

            resp_list = json.loads(resp.data)

            self.assertEqual(resp_list, [], msg="数据信息返回有误")

    def test_query_inserted_data(self):
        with app.app_context():
            data_insert = {"issue": "A", "repo": "A", "branch": "A"}

            create_issue(data_insert)

            data_find = {
                "repo": "A",
                "branch": "A",
            }

            resp = self.client.get("/issue", data=data_find, content_type="application/json")

            resp_dict_list = json.loads(resp.data)

            self.assertIn(data_insert, resp_dict_list, msg="数据信息返回有误")


if __name__ == '__main__':
    unittest.main()
