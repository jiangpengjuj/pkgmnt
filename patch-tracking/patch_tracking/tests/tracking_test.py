import unittest
import json
from patch_tracking.app import app
from patch_tracking.database import reset_db
from patch_tracking.api.business import create_tracking


class TestTracking(unittest.TestCase):
    '''
        tracking测试
    '''
    def setUp(self) -> None:
        self.client = app.test_client()
        reset_db.reset()

    def test_none_data(self):
        with app.app_context():
            data = {
                "repo": "A",
                "branch": "A",
            }

            resp = self.client.get("/tracking", data=data, content_type="application/json")

            resp_list = json.loads(resp.data)
            print("resp_list===", resp_list)
            self.assertEqual(resp_list, [], msg="数据信息返回有误")

    def test_insert_data(self):
        data = {
            "version_control": "A",
            "scm_repo": "A",
            "scm_branch": "A",
            "scm_commit": "A",
            "repo": "A",
            "branch": "A",
            "enabled": 0
        }

        resp = self.client.post("/tracking", json=data, content_type="application/json")
        resp_dict = json.loads(resp.data)
        self.assertEqual(None, resp_dict, msg="数据信息返回有误")

    def test_query_inserted_data(self):
        with app.app_context():
            data_insert = {
                "version_control": "B",
                "scm_repo": "B",
                "scm_branch": "B",
                "scm_commit": "B",
                "repo": "B",
                "branch": "B",
                "enabled": 0
            }

            create_tracking(data_insert)

            data_find = {
                "repo": "B",
                "branch": "B",
            }

            resp = self.client.get("/tracking", data=data_find, content_type="application/json")

            resp_dict_list = json.loads(resp.data)
            print("resp_dict_list===", resp_dict_list)
            self.assertIn(data_insert, resp_dict_list, msg="数据信息返回有误")


if __name__ == '__main__':
    unittest.main()
