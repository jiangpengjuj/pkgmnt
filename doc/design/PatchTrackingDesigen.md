
# 一、背景：

在 openEuler 发行版开发过程，需要及时更新上游社区各个软件包的最新代码，修改功能 bug 及安全问题，确保发布的 openEuler 发行版尽可能避免缺陷和漏洞。


# 二、原始需求 -- 社区补丁跟踪

对软件包管理系统的软件，主动监控上游社区提交，自动生成补丁，并自动提交 issue 给对应的 maintainer，同时自动验证补丁基础功能，减少验证工作量支持 maintainer 快速决策。


# 三、依赖组件

- 开源软件的上游 SCM（包括 Github、Gitlab、SVN）
- 存放包源码的 Gitee Git 仓库


# 四、License

Mulan V2


# 五、功能概述

Gitee 的 src-openEuler 组织下存放软件包的包源码，每个软件包存放在一个 Git 仓库，其中包含构建软件包 source RPMs 以及进一步构建 binary RPMs 所需的文件。
提供接口，允许用户配置所需跟踪的软件及版本，以及对应跟踪的上游社区 SCM 信息（包括仓库、分支、最新处理过的 Commit），当上游源代码有变更时，能够提取这些变更，形成补丁文件，并且在包源码的仓库创建 issue，由 Maintainer 决策是否合并到包源码仓库中，从而能够构建最新的 source RPMs 和 binary RPMs。


# 六、功能设计

- 支持svn/git代码监控
- 自动从svn/git中获取补丁
- 自动提交issue到对应项目
- 自动关联 issue 和 PR
- 自动过滤已经提交issue的清单
*1.如果有社区有lts分支，直接跟踪LTS分支。2.如果没有LTS,主干合入比较少，可以直接跟踪，3.如果主干合入很多的情况下，建议跟踪fedora/centos/suse*


## 6.1 补丁跟踪后台进程提供的 API

- 查询补丁跟踪项

![](API_Tracking1.png)


- 查询补丁跟踪关联的 Issue

![](API_Issue.png)


> 写接口需要监听在独立的 IP 上

- 创建/更新补丁跟踪项

创建、更新补丁跟踪项使用同一个接口，如果“repo”及“branch”指定的跟踪项已经存在则更新，如果不存在则创建新的跟踪项。

![](API_Tracking2.png)

- 设置 token



## 6.2 创建/更新补丁跟踪项命令行

命令行调用 “创建/更新补丁跟踪项” API，创建或更新补丁跟踪项，有 3 种用法：

1. 命令行参数指定补丁跟踪项各参数

```
patch-tracking-cmd.py --server 127.0.0.1:5000 --repo XX --branch XX --version_control github --scm_repo XX --scm_branch XX --enabled XX
```

2. 从 \<repo>.yaml 导入一个补丁跟踪项

```
patch-tracking-cmd.py --server 127.0.0.1:5000 --file <repo>.yaml
```

__\<repo>.yaml__ 文件格式
```
repo: xx
branch: yy
version_control: github
src_repo: MirBSD/mksh
src_branch: 1.x
patch_tracking_enabled: true
```

3. 从 \<dir> 目录批量导入补丁跟踪项

```
patch-tracking-cmd.py --server 127.0.0.1:5000 --dir <dir>
```

__\<dir>__ 目录结构

```
|--<repo1>.yaml
|--<repo2>.yaml
|--<repo3>.yaml
|--<repo4>.yaml
```

校验上游社区仓库及地址是否存在。


## 6.3 补丁跟踪核心流程

![](PatchTracking.jpg)


## 6.4 Maintainer 补丁处理流程

![](Maintainer.jpg)


# 七、数据表设计

- tracking

| 序号 | 名称 | 说明 | 类型 | 键 | 允许空 | 默认值 |
|    - |   - |    - |   - |  - | - | -  |
| 1 | id | 自增补丁跟踪项序号 | Int | | NO | -  |
| 2 | version_control | 上游 SCM 的版本控制系统类型 | String | | NO | - |
| 3 | scm_repo | 上游 SCM 仓库地址 | String | | NO | -  |
| 4 | scm_branch | 上游 SCM 跟踪分支 | String | | NO | -  |
| 5 | scm_commit | 上游代码最新处理过的 Commit ID | String | | NO | -  |
| 6 | repo | 包源码在 Gitee 的仓库地址 | String | Primary | NO | -  |
| 7 | branch | 提交 PR 合入的目标分支 | String | Primary | NO | -  |
| 8 | enabled | 是否启动跟踪 | Bool | | NO | -  |

- issue

| 序号 | 名称 | 说明 | 类型 | 键 | 允许空 | 默认值 |
|    - |   - |    - |   - |  - | - | -  |
| 1 | issue | issue 号 | String | Primary | NO | -  |
| 2 | tracking_id | 关联 tracking 表 id 字段 | String | Foreign | NO | -  |



# 八、配置文件

```
external_server: 1.2.3.4:5000              # 读接口对外监听的地址
internal_server: 127.0.0.1:6000            # 写接口监听的地址
period: 12h                                # 检查上游社区源代码变更的周期
```


# 九、实现

- 开发语言：Python
- 运行环境：Docker 方式运行，优先考虑托管在华为云 CCE 或者 CSE
- 日志：使用通用日志框架，日志信息打屏，由 Docker 引擎或者日志服务保存日志


# 十、遗留问题

- SVN、Git SCM 仓库对应的 API 需要验证
- 暂不考虑 RESTful API 认证问题
- Issue 格式规范：生成补丁文件对应的上游 SCM 的 Commit 信息
- PR 关联对应 Issue
- Issue 关联对应 PR
- 暂用 SQLite，功能完善后切到 MySQL
- 代码实现实现可扩展


# 十一、安全问题

- token 需要有刷新机制 -- 颜小兵、曹志
   补丁跟踪程序提供设置 token RESTful API，先不考虑加密 -- 陈燕潘
- token 的加密方案 -- 颜小兵
- MySQL 密码如何保存 -- 颜小兵
- 了解 Gitee 的安全机制 -- 曹志
- 补丁跟踪使用的 token 建议使用创建 issue 和 PR 的最小权限 -- 曹志



