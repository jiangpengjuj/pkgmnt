from . import Config


class ProductionConfig(Config):
    '''
        @说明    生产模式下的配置
        @时间    2020-05-11 21:01:07
        @作者    gzt
    '''

    # 调试模式
    DEBUG = False
